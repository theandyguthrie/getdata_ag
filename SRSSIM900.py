import visa
import numpy as np
import time

rm = visa.ResourceManager()
rm.list_resources()

class SRSSIM900():
    def __init__(self, GPIB, Settings):

        self.address = GPIB
        self.inst = rm.open_resource(self.address)
        self.inst.write('*RST')
        self.inst.write('CONN '+str(Settings['Port']) + ',"OFF"')
        self.inst.write('*RST')
        self.settings = Settings
        curvolt = float(self.inst.query('VOLT?'))
        if curvolt!=0:
            x = np.linspace(curvolt,0,round(abs(curvolt/0.01)+1))
            for ind in range(len(x)):
                self.inst.write('VOLT '+str(x[ind]))
                time.sleep(0.1)
        self.inst.write('OPON')
        print(['Initialised: ', self.inst.query('*IDN?')])
        self.inst.write('"OFF"')


    def _setCur(self, value):
        self.inst.write('CONN ' + str(self.settings['Port']) + ',"OFF"')
        self.inst.write('*RST')
        self.inst.write('VOLT '+str(value*self.settings['Resist']))
        self.inst.write('"OFF"')

    def _getCur(self):
        self.inst.write('CONN ' + str(self.settings['Port']) + ',"OFF"')
        self.inst.write('*RST')
        x = float(self.inst.query('VOLT?'))/self.settings['Resist']
        self.inst.write('"OFF"')
        return x




