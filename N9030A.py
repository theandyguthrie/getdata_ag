import numpy as np
import visa
import pyvisa.highlevel
import time
import mod_oscillator as osc

class N9030A:

    def __init__(self,Address,Settings):
        rm = visa.ResourceManager()
        rm.list_resources()



        self.dev = rm.open_resource(Address)

        self.dev.write(':INST:SEL SA')


        self.centerfreq = float(Settings['CenterFrequency'])
        self.span = float(Settings['Span'])
        self.points = int(Settings['Points'])


        self.dev.InputBufferSize = 2 ^ 16
        self.dev.OutputBufferSize = 2 ^ 16
        self.dev.Timeout = 100

        # Define output format
        self.dev.write(':FORM:DATA REAL')
        self.dev.write(':FORM:BORD SWAP')


        self.dev.write('INIT:REST')

        # Set frequency.
        self.dev.write(':SENS:FREQ:CENT ', str(Settings['CenterFrequency']))

        # Set frequency span.
        self.dev.write(':SENS:FREQ:SPAN ', str(Settings['Span']))


        # Set number of points
        self.dev.write(':SENS:SWE:POIN ', str(Settings['Points']))

        #Sets the resolution bandwidth
        #self.dev.write(':SENS:BAND:RES ', str(Settings['Bandwidth']))

        # Set Averages
        self.dev.write(':SENS:AVER ON ')

        self.dev.write(':SENS:AVER:COUN ', str(Settings['Averages']))


        #SETS TO IQ MODE - REMOVE FOR NORMAL OPERATION
        #self.dev.write('INST:SEL BASIC')




        # Auto-Scale Data
        #self.dev.write(':DISP:WIND1:TRAC1:Y:AUTO ')
        #self.dev.write(':INIT:CONT ON ')
        print(['Initialised: ', self.dev.query('*IDN?')])
        time.sleep(0.1)

        return

    def GetTrace(self):


        print('Sweeping ' + str(self.dev.query('*IDN?')))

        #Restart measurements
        self.dev.write('SENS:AVER:CLE')
        time.sleep(0.1)
        self.dev.write('INIT:REST')

        # Poll *OPC? for when device is finished sweep, =1 when finished
        a = 0
        while a!= 1:
            try:
                a = int(self.dev.query('*OPC?'))
                time.sleep(0.1)
            except pyvisa.errors.VisaIOError:
                time.sleep(0.5)

        print('Collecting data')

        #Read the trace
        a=0
        while a==0:
            try:
                Signal = self.dev.query_binary_values('CALC:DATA?')
                a=1
            except pyvisa.errors.VisaIOError:
                time.sleep(1)
                a=0



        sReal,sImag,phase,frequency,magnitude  = [],[],[],[],[]


        for x in range(0,len(Signal)):
            if (x%2 == 0):
                frequency.append(Signal[x])
            else:
                sReal.append(float(Signal[x]))
                sImag.append(0)

        for y in range(0, len(sReal)):
            if (sReal != 0):
                phase.append(np.arctan(sImag[y] / sReal[y]))
            else:
                phase.append(np.arctan(0))

        magnitude = sReal
        #magnitude = osc.dbm_to_watts(magnitude)

        frequency = np.linspace(self.centerfreq - (self.span*0.5), self.centerfreq + (self.span*0.5), self.points)


        # Read frequency data

        data = {'Frequency': np.array(frequency), 'sReal': np.array(sReal), 'sImag': np.array(sImag),'Magnitude': np.array(magnitude), 'Phase': np.array(phase)}
        return data


    def GetPower(self):

        out = -9999
        return out

    def GetVoltage(self):

        out = np.double(self.dev.query(':SOUR:POW:LEV:IMM:AMPL?'))
        return out

    def SetCentralFrequency(self, Frequency):

        self.dev.write(':SENS:FREQ:CENT ', str(Frequency))
        time.sleep(0.1)

        out = np.double(self.dev.query(':SENS:FREQ:CENT?'))
        return out

    def GetCentralFrequency(self, Frequency):

        out = np.double(self.dev.query(':SENS:FREQ:CENT?'))
        return out

    def SetSpan(self, Span):

        # Set frequency span.
        self.dev.write(':SENS:FREQ:SPAN ', str(Span))
        time.sleep(0.1)

        out = np.double(self.dev.query(':SENS:FREQ:Span?'))
        return out

