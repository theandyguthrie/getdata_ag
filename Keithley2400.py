import visa
import numpy as np
import time

# By Joshua Chawner
# V1.0 ~ Known to work quite well.

rm = visa.ResourceManager()
rm.list_resources()


class Keithley2400():
    def __init__(self, GPIB, Settings):

        self.address = GPIB
        self.tolerence = Settings['Tolerence']
        self.inst = rm.open_resource(self.address)

        self.dT = 0.05  # seconds per step

        try:
            self.safe_rate = Settings['RampRate']
        except KeyError:
            pass

        self._OUTPUTON()

        self.inst.write(':SENS:AVER 0')
        self.inst.write(':FORM:DATA ASCII')
        self.inst.write(':FORM:ELEM VOLT,CURR')

        # try:
        #     self.inst.write(':SOUR:CURR:RANG ' + str(Settings['Range'])) #1.05E-2')
        # except KeyError:
        #     pass

        print(['Initialised: ', self.inst.query('*IDN?')])

    def ping(self):
        return self.inst.query("*IDN?")

    def _RESET(self):
        self.inst.write("*RST")

    def _set_rate(self, rate):
        self.safe_rate = rate

    def _OUTPUTON(self):
        self.inst.write(":OUTP ON")

    def _READ(self):
        return self.inst.query(":READ?")

    def _getVOLT(self):

        val = float(self.inst.query(':MEAS?')[0:13])
        if val > 1E+37:
            return np.NAN
        else:
            return val


    def _getCURR(self):

        return float(self.inst.query(':SOUR:CURR?'))


    def _measure_CURR(self):

        return float(self.inst.query(':MEAS?')[14:27])

    def _setVOLT(self, value):
        if self.getCurrentMeasurementType() == "VOLT\n":
            self.inst.write(":SOUR:VOLT " + str(value))
        else:
            print("Incorrect Keithley2400 source type! It's outputting CURRENT!!")

    def setVOLT(self, value):



        if self.getCurrentMeasurementType() == "VOLT\n":
            self.inst.write(":SOUR:VOLT " + str(value))
        else:
            print("Incorrect Keithley2400 source type! It's outputting CURRENT!!")

            finishedsweeping = False

            while finishedsweeping==False:
                if ((-1*self.tolerence) <  (self._getVOLT()- value) < (self.tolerence)):
                    finishedsweeping = True
                else:
                    time.sleep(0.1)



    def _setCURR(self, value):

        if self.getCurrentMeasurementType() == "CURR\n":
            self.inst.write(":SOUR:CURR " + str(value))
        else:
            print("Incorrect Keithley2400 source type! It's outputting voltage!!")

    def getCurrentMeasurementType(self):
        return self.inst.query(":SOUR:FUNC?")

    def _RAMP(self, stop):  # ramp from current value to value 'stop' V


        print('Settings voltage to ' + str(stop) + ' on Keithley2400')

        start = self._getVOLT()
        step = self.safe_rate * self.dT * np.sign(stop - start)  # I Keithley min step
        if step != 0:
            points = np.abs((stop - start) / step) + 1
        else:
            points = 0

        if int(points) > 2:
            for i in range(int(points)):
                #if np.abs(self._getVOLT() - (start + i*step)) <= np.abs(step*2):
                self._setVOLT(start + i * step)
                time.sleep(self.dT)
            if np.abs(self._getVOLT() - (start - stop)) <= np.abs(step * 2):
                self._setVOLT(stop)
        else:
            if np.abs(self._getVOLT() - stop) <= np.abs(step * 2):
                self._setVOLT(stop)





    def _RAMP_Current(self, stop):  # ramp from current value to value 'stop' I


        print('Setting current to ' + str(stop) + ' on Keithley2400')

        start = self._getCURR()

        start = float(start)
        stop = float(stop)



        step = self.safe_rate * self.dT * np.sign(np.subtract(float(stop), float(start)))  # V Keithley min step


        if step != 0:
            points = np.abs((np.subtract(stop, start)) / step) + 1
        else:
            points = 0



        if int(points) > 2:
            for i in range(int(points)):
                if np.abs(self._getVOLT() - (start + i*step)) <= np.abs(step*2):
                    self._setCURR(start + i * step)
                    time.sleep(self.dT)
            if np.abs(float(self._getCURR()) - (start + i * step)) <= np.abs(step * 2):
                self._setCURR(stop)
        else:
            if np.abs(float(self._getCURR()) - stop) <= np.abs(step * 2):
                self._setCURR(stop)

    def _DELAY(self, time):  # time in seconds
        self.inst.write(":SOUR:DEL: " + str(time))


