"""
Library for LTL lab to load, convert and save various data files

"""
# import pandas as pd
# import numpy as np
import os
# import subprocess
import re
import time
import h5py


def Output_h5py(Settings, data, Name):
    # Writes data to h5py file. Can be called by data.get()      eg: data.get("Frequency")[x]
    # Method for writing to seperate data file titled with timestamp

    out = h5py.File(Settings['File']['Data'] + '-' + Name + '.hdf5', 'w')

    for keys in data:
        if keys != 'Settings':
            out.create_dataset(keys, data=data[keys])


def OutputLabbook(Settings):
    plots = []
    for f in os.listdir(Settings['File']['PlotDir']):
        if re.match('Plot' + Settings['Time'], f):
            plots.append(f)

    book = open(Settings['File']['Log'] + Settings['File']['Labbook'], 'r+')

    # Removes \end{document} from labbook
    book.seek(0, os.SEEK_END)
    book.seek(book.tell() - 16, os.SEEK_SET)
    book.truncate()
    book.close()

    book = open(Settings['File']['Log'] + Settings['File']['Labbook'], 'a')

    Tex = [r'',
           r'    \section{' + time.strftime("%c") + '}',
           r'    \begin{tabular}{|p{0.6 \textwidth}|p{0.35\textwidth}|}',
           r'        \hline',
           r'        \subsection{' + Settings['Measurements']['MeasurementsType'] + '}', ]

    for y in range(0, len(plots)):
        Tex.append(
            r'        \includegraphics[width=1.0 \linewidth]{' + str(Settings['File']['PlotDir'] + plots[y]) + '}', )

    Tex.extend([r'      \newline \textbf{Program:} GetData.m',
                r'      \newline \textbf{Data file name:}',
                r'      \newline ' + str(Settings['File']['Data']) + '.h5py',
                r'      \newline \textbf{Figure file name:}',
                r'      \newline ' + str(Settings['File']['Plots']) + '.png',
                '       &',
                r'      \begin{flushright}',
                r'          \textbf{\underline{' + str(Settings['Measurements']['MeasurementsDevice']) + '}}',
                '       \end{flushright}',
                '       Line attenuation: ' + str(Settings['Measurements']['LineAttenuation']) + 'dB',
                '       Gain: ' + str(Settings['Measurements']['Gain']) + 'dB',
                r'      \newline \textbf{Instruments:}',
                r'      \newline Temperature is controlled by Lakeshore;',
                r'      \newline Current is obtained by Keithley2400 Source;',
                r'      \newline Data are obtained by AgilentE5071C Network Analyzer;',
                r'      \vspace{1cm}',
                r'      \newline\textbf{Temperature settings:}',
                r'      \newline $T_{start} = ' + str(Settings['Temperature']['Start']) + '\,\mathrm{K}$',
                r'      \newline $T_{stop} = ' + str(Settings['Temperature']['Stop']) + '\,\mathrm{K}$',
                r'      \newline\textbf{Keithley Current settings:}',
                r'      \newline\textbf{Magnet settings:}',
                r'      \newline\textbf{Network analyzer settings:}',
                r'      \newline $f_{centre} =' +
                str(Settings['NetworkAnalyzer']['CenterFrequency'] / 1e6) + '\,\mathrm{MHz}$',
                r'            \newline $f_{span} =' +
                str(Settings['NetworkAnalyzer']['Span'] / 1e6) + '\,\mathrm{MHz}$',
                r'            \newline Points: $' +
                str(Settings['NetworkAnalyzer']['Points']) + '$',

                r'            \newline Bandwidth: $' +
                str(Settings['NetworkAnalyzer']['IFBW']) + '\,\mathrm{Hz}$',
                r'      \\',
                '       \hline',
                '       \end{tabular}',
                '\end{document}'])

    for x in range(0, len(Tex)):
        book.write(str(Tex[x]))
        book.write("\n")

    book.close()


def OutputLabbook1(Settings):
    plots = []
    for f in os.listdir(Settings['File']['PlotDir']):
        if re.match('Plot' + Settings['Time'], f):
            plots.append(f)

    book = open(Settings['File']['Log'] + Settings['File']['Labbook'], 'r+')

    # Removes \end{document} from labbook
    book.seek(0, os.SEEK_END)
    book.seek(book.tell() - 16, os.SEEK_SET)
    book.truncate()
    book.close()

    book = open(Settings['File']['Log'] + Settings['File']['Labbook'], 'a')

    Tex = [r'',
           r'       \section{' + time.strftime("%c") + '}',
           r'       \begin{tabular}{|p{0.7 \textwidth}|p{0.25\textwidth}|}',
           r'       \hline',
           r'       \subsection{' + Settings['Measurements']['MeasurementsType'] + '}', ]

    for y in range(0, len(plots)):
        Tex.append(r'      \includegraphics[width=1.0 \linewidth]{' +
                   str(Settings['File']['PlotDir'] + plots[y]) + '}', )

    Tex.extend([r'      \newline \textbf{Program:} GetData.py',
                r'      \newline \textbf{Data file name:}',
                r'      \newline ' + str(Settings['File']['Data']) + '.h5py',
                r'      \newline \textbf{Figure file name:}',
                r'      \newline ' + str(Settings['File']['Plots']) + '.png',
                '       &	',
                r'      \begin{flushright}',
                r'      \textbf{\underline{' + str(Settings['Measurements']['MeasurementsDevice']) + '}}',
                '       \end{flushright}',
                r'      \textbf{Circuit}',
                r'      \newline Line attenuation: ' + str(Settings['Measurements']['LineAttenuation']) + 'dB',
                r'      \newline Gain: ' + str(Settings['Measurements']['Gain']) + '',
                r'      \newline \textbf{Instruments:}',
                ])

    for key in Settings['Devices'].keys():
        for param in Settings['Devices'][key]['Parameters'].keys():
            Tex.append(r'       \newline ' + param + r' is controlled by ' + key)

    Tex.append(r'       \newline Signal is obtained on ' + str(Settings['Device_to_Frequency_Sweep']))
    Tex.append(r'       \newline \newline \textbf{Parameters:}')

    for key in Settings['Devices'].keys():
        Tex.append(r'       \newline \textbf{' + key + '}')

        for param in Settings['Devices'][key]['Settings'].keys():
            Tex.append(r'       \newline ' + param + ' =  $' + str(Settings['Devices'][key]['Settings'][param]) + '$')
            Tex.append(r'       \newline')

        for param in Settings['Devices'][key]['Parameters'].keys():
            if Settings['Devices'][key]['Parameters'][param]['Array'] is not None:
                Tex.append(r'       \newline ' + param + r'$_{\mathrm{start}} =  ' +
                           str(Settings['Devices'][key]['Parameters'][param]['Array'][0]) + '$')

                if len(Settings['Devices'][key]['Parameters'][param]['Array']) > 1:
                    Tex.append(r'       \newline ' + param + r'$_{\mathrm{end}} =  ' +
                               str(Settings['Devices'][key]['Parameters'][param]['Array'][-1]) + '$')
                    Tex.append(r'       \newline ' + param + r'$_{\mathrm{points}} =  ' +
                               str(len(Settings['Devices'][key]['Parameters'][param]['Array'])) + '$')

    # Tex.extend([
    #             r'        \newline\textbf{Temperature settings:}',
    #             r'            \newline $T_{\mathrm{start}} = ' +
    #             str(Settings['Temperature']['Start']) + '\,\mathrm{K}$',
    #             r'            \newline $T_{\mathrm{stop}} = ' +
    #             str(Settings['Temperature']['Stop']) + '\,\mathrm{K}$',
    #             r'        \\',
    #             '        \hline',
    #             '    \end{tabular}'])

    Tex.extend([r'        \newline\textbf{Temperature settings:}'])

    for key in Settings['Temperature'].keys():
        Tex.append(r'\newline $T_{\mathrm{' + key + '}} = ' + str(Settings['Temperature'][key]) + '\,\mathrm{K}$')

    Tex.extend([r'      \\',
                '       \hline',
                '       \end{tabular}'])

    Tex.append(r'\end{document}')

    for x in range(0, len(Tex)):
        book.write(str(Tex[x]))
        book.write("\n")

    book.close()
