import numpy as np
import visa
import time


class NetworkAnalyzer:
    def __init__(self, Address, Settings):
        rm = visa.ResourceManager()
        rm.list_resources()
        self.dev = rm.open_resource(Address)
        #   Stops the beeping!
        self.dev.write(':SYST:BEEP:WARN:STAT OFF')
        #   Setup display to show two sweeps of s21, LogMag and Phase
        self.dev.write(':DISP:SPL D1')
        self.dev.write('CALC:PAR:COUN 2')
        self.dev.write(':DISP:WIND:SPL D1_2')
        self.dev.write(':CALC1:PAR1:DEF s21')
        self.dev.write(':CALC1:PAR2:DEF s21')
        self.dev.write(':CALC1:TRAC1:FORM MLOG')
        self.dev.write(':CALC1:TRAC2:FORM PHAS')

        #   Define output format
        self.dev.write(':FORM:DATA REAL')
        self.dev.write(':FORM:BORD SWAP')

        #   Stop automatic triggers
        self.dev.write(':INIT:CONT OFF')
        self.dev.write(':TRIG:AVER ON')
        self.dev.write(':TRIG:SOUR BUS')

        #   Set linear frequency sweep.
        self.dev.write(':SENS:SWE:TYPE LIN')

        #   Set sweep time to auto.
        try:
            self.dev.write(':SENS:SWE:TIME:AUTO OFF')
            self.dev.write(':SENS:SWE:TIME ' + str(Settings['SweepTime']))
        except KeyError:
            self.dev.write(':SENS:SWE:TIME:AUTO OFF')

        #   Set sweep delay to zero.
        self.dev.write(':SENS:SWE:DEL 0')

        #   Set frequency.
        try:
            self.dev.write(':SENS:FREQ:CENT ', str(Settings['CenterFrequency']))
        except KeyError:
            pass

        #   Set frequency span.
        try:
            self.dev.write(':SENS:FREQ:SPAN ', str(Settings['Span']))
        except KeyError:
            pass

        #   Set number of points
        self.dev.write(':SENS:SWE:POIN ', str(Settings['Points']))

        #   Set IF Bandwidth
        self.ifbw = Settings['IFBW']
        self.dev.write(':SENS:BAND ', str(Settings['IFBW']))

        # Set Averages
        self.dev.write(':SENS:AVER ON ')

        self.dev.write(':SENS:AVER:COUN ', str(Settings['Averages']))

        self.dev.write(':TRIG:AVER ON')

        self.dev.write(':TRIG:SEQ:AVER ON')


        # Turn Smoothing Off
        self.dev.write(':CALC:SMO OFF ')

        # Auto-Scale Data
        self.dev.write(':DISP:WIND1:TRAC1:Y:AUTO ')
        self.dev.write(':INIT:CONT ON ')

        self.name = self.dev.query('*IDN?')
        print(['Initialised: ', self.name])
        return

    def GetTrace(self):
        # Trigger Measurements
        print('Sweeping ' + self.name)
        self.dev.write(':SENS:AVER:CLE')
        #self.dev.write(':INIT:CONT ON ')
        self.dev.write(':TRIG:SING')


        # Measurement completion signalled by setting bit 4 (value=16) in operation status register ... so poll for that condition
        if self.ifbw < 1e5:
            while (np.int((self.dev.query(':STAT:OPER:COND?'))) != 32):
                pass
        else:
            time.sleep(0.1)


        # Read the trace
        Signal = self.dev.query_binary_values(':CALC:DATA:SDAT?', datatype='d')


        phase = []


        sReal = Signal[::2]
        sImag = Signal[1::2]
        magnitude = np.sqrt(np.power(sImag, 2) + np.power(sReal, 2))




        # Read frequency data
        frequency = self.dev.query_binary_values(':SENS:FREQ:DATA?', datatype='d')
        data = {'Frequency': np.array(frequency), 'sReal': np.array(sReal), 'sImag': np.array(sImag),'Magnitude': np.array(magnitude), 'Phase': np.array(phase)}

        print('Finished Sweeping ' + self.name)

        return data


    def SetPower(self, Power):

        print('Setting power to ' + str(Power) + 'dBm on ' + self.name)

        self.dev.write(':SOUR:POW:LEV:IMM:AMPL ', str(Power))



    def GetPower(self):
        return np.double(self.dev.query(':SOUR:POW:LEV:IMM:AMPL?'))


    def SetCentralFrequency(self, Frequency):

        print('Setting center frequency to ' + str(Frequency) + 'Hz on ' + self.name)
        self.dev.write(':SENS:FREQ:CENT ', str(Frequency))


    def GetCentralFrequency(self):
        return np.double(self.dev.query(':SENS:FREQ:CENT?'))


    def SetOutput(self,output):
        print('Setting RF Out to '+ str(output))
        self.dev.write('OUTP:STAT ' + str(output))


    def GetOutput(self):

        return np.int(self.dev.query('OUTP:STAT?'))




    def SetSweepTime(self,sweeptime):

        self.dev.write(':SENS:SWE:TIME:AUTO 0')
        self.dev.write(':SENS:SWE:TIME ' + str(sweeptime))



    def SetSpan(self,span):
        print('Settings span to ' + str(span))
        self.dev.write(':SENS:FREQ:SPAN ' + str(span))

    def GetSpan(self):

        return self.dev.query(':SENS:FREQ:SPAN?')


    def SetAverages(self,aver):
        self.dev.write(':SENS:AVER:COUN ', str(aver))




