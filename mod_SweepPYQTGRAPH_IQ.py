import sys
from PyQt5 import QtWidgets, QtCore, QtGui
import pyqtgraph as pg
from mod_fileio_1 import Output_h5py,OutputLabbook1
import pyqtgraph.exporters
import numpy as np
import time
import scipy.signal
import h5py

#Class for the plotting software using pyqt
class plot_app(QtWidgets.QWidget):


    #Takes the Devices dict Settings dicts and stores these in the class.
    def __init__(self,Settings, Devices = None ,Name = ''):
        # Setup GUI

        super(plot_app, self).__init__()

        self.Settings = Settings
        self.Devices = Devices
        self.Name = Name


        self.init_ui()
        self.show()


        self.go()

    #Creates all the basic graphs. One colormap with histogram and one standard plot.
    def init_ui(self):

        self.t0 = time.time()
        self.last_ind_plot = 0


        #Sets colors and fonts for plots
        pg.setConfigOptions(background='w',foreground='k')
        labelStyle = {'font-family': "calibri",'font-size': '18px'}
        self.labelStyle = labelStyle
        titleStyle = {'size': '18px'}
        self.titleStyle = titleStyle
        font = QtGui.QFont()
        font.setPixelSize(15)


        #Creates colormap style plot (called ImageItem in pyqtgraph). Puts this plot inside a widget
        img1 = pg.ImageItem()
        plot_widget = pg.PlotWidget()
        plot_widget.addItem(img1)
        self.img1 = img1
        self.main_plot = plot_widget.getPlotItem()


        # Sets title to measurement type from settings
        self.main_plot.showButtons()
        plot_widget.setLabel('left', 'Frequency (Hz)',**labelStyle)
        plot_widget.setTitle(self.Settings['Measurements']['MeasurementsType'],**titleStyle)

        # Get plot widget for plot 2 (regular line plot)
        plot_widget2 = pg.PlotWidget()
        self.plot2 = plot_widget2.getPlotItem()


        #Sets yaxis label to the ['SignalLabel'] from the device being swept. (i.e |S21| for network analyzers)
        #Sets x axis label
        self.plot2.showButtons()
        Signallabel = self.Devices[self.Settings['Device_to_Frequency_Sweep']][1]['Settings']['SignalLabel']
        plot_widget2.setLabel('left', Signallabel,**labelStyle)
        plot_widget2.setLabel('bottom', 'Frequency (Hz)',**labelStyle)



        #Hist object is the sliding color scale in pyqtgraph. You can change the colors by changing loadPreset
        #Puts the hist object inside a widget.
        hist = pg.HistogramLUTWidget()
        hist.setImageItem(img1)
        hist.gradient.loadPreset('spectrum')


        # Define layouts and sets location and size
        # Define layouts and sets location and size
        win = QtGui.QWidget()
        self.win = win
        self.setGeometry(2880,30,960,1170)



        #Creates a grid inside the large widget. Puts first plot in row1,col1, plot2 in row2, col1, and hist in col2.
        #Scales col1 so it is 15x bigger than col2. (Makes the colorbar look good)
        layout = QtGui.QGridLayout()
        win.setLayout(layout)
        self.setLayout(layout)
        layout.addWidget(plot_widget, 0, 0, 1, 1)
        layout.addWidget(plot_widget2, 1, 0, 1, 1)
        layout.addWidget(hist, 0, 1, 2, 1)
        layout.setColumnStretch(0, 15)
        layout.setColumnStretch(1, 1)



        #Sets fonts to the font defined above
        plot_widget.getAxis("bottom").tickFont = font
        plot_widget.getAxis("bottom").labelFont = font
        plot_widget.getAxis("left").tickFont = font
        plot_widget2.getAxis("bottom").tickFont = font
        plot_widget2.getAxis("left").tickFont = font

        self.NumberofTicks = 10
        self.TickDividers = [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000]







    #This will be started as a thread by initui and controls the plotting.
    #The pyqtSlot defines the data types that are taken from the other thread
    #These are case straight to the variables in the def(...)
    @QtCore.pyqtSlot(np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray, str, int, int, str)
    def plot(self, x, yMag, yReal, yImag, fMag, fReal,fImag, label, ind, arrayLength, timeLeftString):

        self.ind = ind


        # Finds upper and lower bounds of the zdata
        plot_min_z = np.amin(yMag)
        plot_max_z = np.amax(yMag)

        #Sets the axis labels to the xdata parameter data
        self.main_plot.setLabel('bottom', label, **self.labelStyle)
        self.plot2.setTitle('Plot ' + str(self.ind + 1) + ' of ' + str(arrayLength) + ' - ' + timeLeftString,**self.titleStyle)


        #Scales the image on the colormap by the size of the frequency and xparam data
        self.img1.resetTransform()
        f_scale = (np.amax(fMag) - np.amin(fMag)) / yMag.shape[1]
        x_scale = 1# (np.amax(x) - np.amin(x)) / yMag.shape[0]
        x_offset = -0.5 #np.amin(x)
        f_offset = np.amin(fReal)


        #Moves the raw rectangle to the lowest x value and f values.
        #Scales by the actual span of each of the axes
        self.img1.translate(x_offset, f_offset)
        self.img1.scale(x_scale,f_scale)




        #Plots data
        self.img1.setImage(yMag, levels=[plot_min_z,plot_max_z], autolevels=False)

        #plots last sweep as a function of frequency
        curve1 = self.plot2.plot()
        curve2 = self.plot2.plot()
        curve1.setData(fReal, yReal[-1], clear=True, pen=(255,165,0))
        curve2.setData(fImag, yImag[-1], clear=True, pen=(0, 255, 0))

        # Swaps the 'run' labels for the actual x values as strings. This was done to allows up and down sweeping for the color plot
        # if len(x) > 1:
        #     Strings = self.main_plot.getAxis('bottom').tickStrings(x, 1, np.abs(x[0] - x[1]))
        #     tickStrings = [[]]
        #     for index in range(0, ind + 1):
        #         tickStrings[0].append((index, Strings[index]))
        #
        #     tickStringstemp = tickStrings[0]
        #     ind = 0
        #     while len(tickStrings[0]) > self.NumberofTicks:
        #         tickStrings[0] = tickStringstemp
        #         tickStrings[0] = tickStrings[0][::self.TickDividers[ind]]
        #
        #         ind += 1
        #
        #     self.main_plot.getAxis('bottom').setTicks(tickStrings)


        #Saves image of both plots once every 10 seconds
        if self.last_ind_plot != self.ind:
            print('saving plots')
            self.last_ind_plot = self.ind
            self.saveImages()





    #Starts the three threads and connects the slow to the signal
    def go(self):
        self.th1 = savingThread({},{},'')


        self.th = random_thread(self.Settings ,self.th1, self.Devices,self.Name)
        self.th.new_data.connect(self.plot) # Connect thread signal to plot function in main GUI



        self.th.datatosave.connect(savingThread.run)

        self.th.start()



    #Updates plot and saves two images to the locations specified in the file. Here you can change the x and y size of the plots.
    def saveImages(self):


        QtGui.QApplication.processEvents()

        self.exporter = pg.exporters.ImageExporter(self.main_plot)
        self.exporter2 = pg.exporters.ImageExporter(self.plot2)

        # set export parameters if needed
        self.exporter.params.param('width').setValue(960, blockSignal=self.exporter.widthChanged)
        self.exporter.params.param('height').setValue(500, blockSignal=self.exporter.heightChanged)
        self.exporter2.params.param('width').setValue(960, blockSignal=self.exporter2.widthChanged)
        self.exporter2.params.param('height').setValue(500, blockSignal=self.exporter2.heightChanged)
        # save to file


        self.exporter.export(str(self.Settings['File']['Plots'] + self.Name + '3D.png'))
        self.exporter2.export(str(self.Settings['File']['Plots'] + self.Name + '2D.png'))





#This object contains the thread for getting the data.
class random_thread(QtCore.QThread):

    new_data = QtCore.pyqtSignal(np.ndarray, np.ndarray,np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray, str, int, int, str)
    datatosave = QtCore.pyqtSignal(dict, dict, str)

    def __init__(self, Settings, savingThread, Devices = None ,Name = ''):
        super(random_thread, self).__init__()
        self.Devices = Devices
        self.Settings = Settings
        self.Name = Name
        self.savingThread = savingThread



    def run_old(self):
        #
        frequency = []
        sReal = []
        sImag = []
        magnitude = []
        phase = []
        run = []
        time1 = []
        sTime = []
        bandwidth = []

        Devices = self.Devices
        Settings = self.Settings
        Name = self.Name


        welchReal, welchImag, welchMag = [],[],[]

        x = {}
        Array = []



        # Find which parameter array has length > 1
        for key in list(Devices.keys()):
            for param in Devices[key][1]['Parameters'].keys():
                if len(Devices[key][1]['Parameters'][param]['Array']) > 1:
                    if len(Array) == 0:
                        Array = [key, Devices[key][1]['Parameters'][param]]
                        label = param
                    else:
                        print('This code only supports 2D sweeps')
                        sys.exit()
                x[param] = []
        ind = 0
        if Array == []:
            Array = ['', {'Array': [0]}]
            label = 'Run'

        t0 = int(time.time())

        # Loops for length of the control array
        while ind < len(Array[1]['Array']):



            run.append(ind)

            if ind == 0:
                print('Saving to ' + str(Settings['File']['Data']))

                print('Setting all parameters to their starting values:')
                # Sets all device parameters to their starting values
                for key in list(Devices.keys()):
                    for param in Devices[key][1]['Parameters'].keys():
                        method_to_call = getattr(Devices[key][0], Devices[key][1]['Parameters'][param]['SetFunction'])
                        method_to_call(Devices[key][1]['Parameters'][param]['Array'][0])

            # Sets control parameter once per sweep

            if Array[0] == '':
                Array[1]['Array'].append(1)
            else:
                method_to_call = getattr(Devices[Array[0]][0], Array[1]['SetFunction'])
                method_to_call(Array[1]['Array'][ind])
                #negative
                #z=[ -6.07043012e-11, 3.27893480e-07, -6.73475845e-05, -1.43698402e-03, 3.20876355e-02, 1.24862530e+00, 5.36723368e+00, -1.06578999e+02, 3.30839904e+09]
                #positive
                #z = [2.78690344e-09, 3.48942173e-07, -7.48014905e-05, -1.45444588e-03, 3.91185842e-02, 1.23733499e+00 ,2.08848999e+00, -1.02721771e+02, 3.30859640e+09]
                #200mK positive
                z = [1.71740961e-08,   1.38068361e-07, - 1.19786081e-04, - 5.55782441e-04, 7.73091136e-02,   5.64614617e-01, - 9.49835256e+00, - 9.16060189e+01, 3.30856345e+09]
                #200mK negative
                #z = [1.52966903e-08,   1.76175305e-07,  -1.15056535e-04,  -6.88460981e-04, 7.28272515e-02,   6.90955463e-01,  -8.01254367e+00,  -1.15837841e+02, 3.30837444e+09]
                p = np.poly1d(z)
                freqround=round((p(Array[1]['Array'][ind])))
                Devices['E8257D'][0].SetFrequency(freqround)
                Devices[Settings['Device_to_Frequency_Sweep']][0].SetCentralFrequency(freqround)



            #Reads all instrument parameters to a dictionary
            for key in Devices.keys():
                for param in Devices[key][1]['Parameters'].keys():
                    x[param].append(getattr(Devices[key][0], Devices[key][1]['Parameters'][param]['GetFunction'])())

            #Reads Trace on the device being swept
            data1 = Devices[Settings['Device_to_Frequency_Sweep']][0].GetTrace()
            bandwidth.append(Devices[Settings['Device_to_Frequency_Sweep']][0].ReadBandwidth())


            # Appends all networkanalyzer values to the output
            frequency.append(data1['Frequency'])
            sReal.append(data1['sReal'])
            sImag.append(data1['sImag'])
            magnitude.append(data1['Magnitude'])
            phase.append(data1['Phase'])
            sTime.append(data1['sTime'])
            time1.append(int(time.strftime("%Y%m%d%H%M%S")))

            sampleRate = Devices[Settings['Device_to_Frequency_Sweep']][0].GetSampleRate()

            #Creates dict of all the data to output
            data = {'Frequency': frequency, 'sReal': sReal, 'sImag': sImag, 'Phase': phase, 'Magnitude': magnitude,
                    'Run': run, 'Time1': time1, 'sTime': sTime, 'Bandwidth': bandwidth, 'SampleRate': sampleRate,
                    'Settings': Settings}

            # Adds a value to the output dictionary for each of the instrument parameters
            for key in (x.keys()):
                data[key] = x[key]



            sweeptime = (time.time() - t0) / (ind + 1)

            #Outputs data to labbook and saves first f-sweep for presentation
            if (ind == 3):
                print('Outputting to labbook')
                OutputLabbook1(Settings)

            Timeleft = np.round(sweeptime * (len(Array[1]['Array']) - (ind + 1)), 2)

            if Timeleft < 60:
                Timeleft = np.round(Timeleft, 2)
                timeLeftString = str(Timeleft) + ' seconds left'
            if Timeleft > 3600:
                Timeleft = np.round(Timeleft / 3600, 2)
                timeLeftString = str(Timeleft) + ' hours left'
            if (Timeleft < 3600) and (Timeleft > 60):
                Timeleft = np.round(Timeleft / 60, 2)
                timeLeftString = str(Timeleft) + ' minutes left'


            f1,pxxReal = scipy.signal.welch(data1['sReal'],sampleRate)
            welchReal.append(pxxReal)
            f2, pxxImag = scipy.signal.welch(data1['sImag'], sampleRate)
            welchImag.append(pxxImag)
            fmag, pxxMag = scipy.signal.welch(data1['Magnitude'],sampleRate)
            welchMag.append(pxxMag)


            #Send data back to the plotting thread
            self.new_data.emit(np.array(data[label]), np.array(welchMag), np.array(welchReal), np.array(welchImag), np.array(fmag), np.array(f1), np.array(f2), label, ind, len(Array[1]['Array']), timeLeftString)  # Emit the signal to signal the main gui to do plotting
            #Sets the data in the saving class to the new data
            self.savingThread.setStuff(Settings, data, Name)
            #Saves the data
            self.savingThread.start()

            ind+=1


    def run(self):
        #
        frequency = []
        # sReal = []
        # sImag = []
        # magnitude = []
        # phase = []
        run = []
        time1 = []
        sTime = []
        bandwidth = []
        samplingRate = []

        Devices = self.Devices
        Settings = self.Settings
        Name = self.Name


        welchReal, welchImag, welchMag = [],[],[]

        x = {}
        Array = []



        # Find which parameter array has length > 1
        for key in list(Devices.keys()):
            for param in Devices[key][1]['Parameters'].keys():
                if Devices[key][1]['Parameters'][param]['Array']!= None:
                    if len(Devices[key][1]['Parameters'][param]['Array']) > 1:
                        if len(Array) == 0:
                            Array = [key, Devices[key][1]['Parameters'][param]]
                            label = param
                        else:
                            print('This code only supports 2D sweeps')
                            sys.exit()
                x[param] = []
        ind = 0
        if Array == []:
            Array = ['', {'Array': [0]}]
            label = 'Run'

        t0 = int(time.time())

        # Loops for length of the control array
        while ind < len(Array[1]['Array']):



            run.append(ind)

            if ind == 0:
                print('Saving to ' + str(Settings['File']['Data']))

                print('Setting all parameters to their starting values:')
                # Sets all device parameters to their starting values
                for key in list(Devices.keys()):
                    for param in Devices[key][1]['Parameters'].keys():
                        if Devices[key][1]['Parameters'][param]['SetFunction']!=None:
                            method_to_call = getattr(Devices[key][0], Devices[key][1]['Parameters'][param]['SetFunction'])
                            method_to_call(Devices[key][1]['Parameters'][param]['Array'][0])

            # Sets control parameter once per sweep

            if Array[0] == '':
                Array[1]['Array'].append(1)
            else:
                method_to_call = getattr(Devices[Array[0]][0], Array[1]['SetFunction'])
                method_to_call(Array[1]['Array'][ind])
                #negative
                #z=[ -6.07043012e-11, 3.27893480e-07, -6.73475845e-05, -1.43698402e-03, 3.20876355e-02, 1.24862530e+00, 5.36723368e+00, -1.06578999e+02, 3.30839904e+09]
                #positive
                #z = [2.78690344e-09, 3.48942173e-07, -7.48014905e-05, -1.45444588e-03, 3.91185842e-02, 1.23733499e+00 ,2.08848999e+00, -1.02721771e+02, 3.30859640e+09]
                #200mK
                #z = [1.52966903e-08,   1.76175305e-07,  -1.15056535e-04,  -6.88460981e-04, 7.28272515e-02,   6.90955463e-01,  -8.01254367e+00,  -1.15837841e+02, 3.30837444e+09]
                #500mK negative
                #z = [2.51308786e-08, 7.55350550e-10, -1.61621982e-04, -1.08403805e-04, 1.60038447e-01, 3.30165361e-01, -4.40028416e+01, -1.86501630e+02, 3.30824381e+09]
                #500mK positive
                #z = [  2.69185635e-08, -2.46231966e-08, -1.65815231e-04, -3.57832901e-05, 1.63202905e-01, 2.72100851e-01, -4.44888872e+01, -1.81597425e+02, 3.30841890e+09]
                #1.4K negative
                #z = [ -1.36286964e-08, -2.63932026e-08, 1.17182819e-05, -6.83250939e-05, 8.76835782e-03, 2.31444748e-01, -6.88999954e+00, -7.52036963e+01, 3.30424064e+09]
                #1.4K positive
                #z = [ -1.35144724e-08, -4.82816169e-08, 8.54803907e-06, 1.07632175e-05, 1.45409147e-02, 1.29688585e-01, -9.88276798e+00, -2.90264158e+01, 3.30466490e+09]
                #1.6K negative
                #z = [ -3.46926282e-08, 3.94926614e-07, 1.44625566e-04, -2.29617051e-03, -2.74161932e-01, 3.08885729e+00, 2.32898927e+02, 6.87362463e+02, 3.30127743e+09]
                #10mK
                #z = [1.69537683e-08, -1.22191855e-07, -1.05609228e-04, 4.27810512e-04, 6.33385020e-02, -5.03101545e-01, -3.31017635e+00, 1.14905771e+02, 3.30838984e+09]
                #180mK
                #z = [  1.25447746e-08, 5.12433761e-08, -8.79218583e-05, 9.84712336e-05, 3.61873947e-02, -9.75339896e-02, 1.31892075e+01, 5.55843299e+01, 3.30836946e+09]
                #700mK
                #z = [4.85941236e-09, -1.48189446e-07, -9.17394209e-05, 4.94399028e-04, 1.11372975e-01, -8.78084322e-02, -3.57436604e+01, -1.23983807e+02, 3.30807894e+09]
                #400mK
                #z = [1.42556143e-08, 1.50328932e-07, -1.24364385e-04, -1.54207071e-04, 1.37369298e-01, 3.72544758e-01, -3.98185452e+01, -2.44986565e+02, 3.30831120e+09]
                #950mK
                #z = [-1.29000467e-09, -7.67357837e-08, -5.44738015e-05, 8.28480576e-05, 9.10573748e-02, 1.74641686e-01, -3.79899028e+01, -2.20108754e+02, 3.30754809e+09]
                #1.25K
                #z = [ -2.24438936e-08, 3.60079888e-07, 4.83009835e-05, -1.46825640e-03, -4.46224462e-02, 1.82548965e+00, 2.46740613e+01, -1.06337776e+03, 3.30581277e+09]
                #1.4K
                #z = [-1.89629821e-08, 2.69831541e-07, 3.79459166e-05, -4.75758346e-04, -3.08841091e-02, 9.23493463e-02, 1.35081669e+01,  6.53315344e+01, 3.30368966e+09]
                #1.7K
                #z = [-1.53710943e-08, 2.34828704e-07, 2.69997943e-05, -6.32539715e-04, -1.03499542e-02, 3.00644932e-01, -5.16752186e+00, 1.71750779e+02, 3.29837383e+09]
                #180mK
                #z = [2.04890745e-08, 2.04145977e-07, -1.22032683e-04, -9.20805888e-04, 7.20679572e-02, 1.44620780e+00, 1.99103219e+00, -5.16154888e+02, 3.30837786e+09]
                #10mK
                #z = [5.47431762e-09, -2.16850441e-08, -7.27502104e-05, -1.06880329e-04, 1.07587580e-02, 3.18171729e-01, 1.49387100e+01, -1.28953056e+02, 3.30839511e+09]
                # p = np.poly1d(z)
                # freqround=round((p(Array[1]['Array'][ind])))

                #freqround = Devices['E8257D'][0].GetFrequency()
                #Devices['E8257D'][0].SetFrequency(freqround)
                #Devices[Settings['Device_to_Frequency_Sweep']][0].SetCentralFrequency(2.1391e6)



            # Reads all instrument parameters to a dictionary
            for key in Devices.keys():
                for param in Devices[key][1]['Parameters'].keys():
                    x[param].append(getattr(Devices[key][0], Devices[key][1]['Parameters'][param]['GetFunction'])())

            #Reads Trace on the device being swept
            data = Devices[Settings['Device_to_Frequency_Sweep']][0].GetTrace()


            # Appends all Trace values to the output
            time1.append(int(time.strftime("%Y%m%d%H%M%S")))
            bandwidth.append(Devices[Settings['Device_to_Frequency_Sweep']][0].ReadBandwidth())
            samplingRate.append(Devices[Settings['Device_to_Frequency_Sweep']][0].GetSampleRate())
            frequency.append(data['Frequency'])
            data['Frequency'] = frequency


            data['Time1'] = time1
            data['SampleRate'] = samplingRate
            data['Bandwidth'] = bandwidth
            data['Run'] = run

            #
            # #Creates dict of all the data to output
            # data = {'Frequency': frequency, 'sReal': sReal, 'sImag': sImag, 'Phase': phase, 'Magnitude': magnitude,
            #         'Run': run, 'Time1': time1, 'sTime': sTime, 'Bandwidth': bandwidth, 'SampleRate': sampleRate,
            #         'Settings': Settings}

            # Adds a value to the output dictionary for each of the instrument parameters
            for key in (x.keys()):
                data[key] = x[key]



            sweeptime = (time.time() - t0) / (ind + 1)

            #Outputs data to labbook and saves first f-sweep for presentation
            if (ind == 3):
                print('Outputting to labbook')
                OutputLabbook1(Settings)

            Timeleft = np.round(sweeptime * (len(Array[1]['Array']) - (ind + 1)), 2)

            if Timeleft < 60:
                Timeleft = np.round(Timeleft, 2)
                timeLeftString = str(Timeleft) + ' seconds left'
            if Timeleft > 3600:
                Timeleft = np.round(Timeleft / 3600, 2)
                timeLeftString = str(Timeleft) + ' hours left'
            if (Timeleft < 3600) and (Timeleft > 60):
                Timeleft = np.round(Timeleft / 60, 2)
                timeLeftString = str(Timeleft) + ' minutes left'


            f1,pxxReal = scipy.signal.welch(data['sReal'],data['SampleRate'][0])
            welchReal.append(pxxReal)
            f2, pxxImag = scipy.signal.welch(data['sImag'], data['SampleRate'][0])
            welchImag.append(pxxImag)
            fmag, pxxMag = scipy.signal.welch(data['Magnitude'],data['SampleRate'][0])
            welchMag.append(pxxMag)


            #Send data back to the plotting thread
            self.new_data.emit(np.array(data[label]), np.array(welchMag), np.array(welchReal), np.array(welchImag), np.array(fmag), np.array(f1), np.array(f2), label, ind, len(Array[1]['Array']), timeLeftString)
            self.save(Settings,data,Name='')
            self.save(Settings, data, Name='1')
            ind+=1

    def save(self,Settings,data,Name=''):

        if data['Run'][-1]==0:
            out = h5py.File(Settings['File']['Data'] + '-' + Name + '.hdf5', 'w')
            for key in data:
                if type(data[key])==list:

                    out.create_dataset(key, data=data[key], chunks=True, maxshape=(None,))
                else:
                    out.create_dataset(key, data=[data[key]], chunks=True, maxshape=(None,None))

            out.close()

        else:
            out = h5py.File(Settings['File']['Data'] + '-' + Name + '.hdf5', 'r+')
            for key in data:
                if  len(out[key].maxshape) == 1:
                    out[key].resize((len(data[key])), axis=0)
                    out[key][:] = data[key]
                else:
                    out[key].resize((len(data['Run']), len(data[key])))
                    out[key][-1] = data[key]

            out.close()










class savingThread(QtCore.QThread): # Typically needs to be a PyQt object for signals to work
    def __init__(self,Settings,data,Name):
        super(savingThread, self).__init__()
        self.Settings = Settings
        self.data = data
        self.Name = Name






    @QtCore.pyqtSlot(dict, dict, str)
    def setStuff(self,Settings,data,Name):
        self.Settings = Settings
        self.data = data
        self.Name = Name



    def run(self):

        if self.data:
            Output_h5py(self.Settings,self.data,self.Name)
            Output_h5py(self.Settings, self.data, self.Name + '1')













