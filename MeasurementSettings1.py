import numpy as np
import time
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# Dictionary containing all settings

def MeasurementSettings1():
    Measurements = {'MeasurementsDevice': 'NEMS from Grenoble',
                    'MeasurementsType': r'5T Looking for resonance',
                    'Connectors': '7, 8',
                    'LineAttenuation': '-58',
                    'Gain': '76'}
    # README
    # . This code is designed to perform Network Analyzer f-sweeps as a function of any parameter.
    # . Any parameter will be set according to the [value] in the Array column.
    # . It must be in square brackets. If the array is longer than 1 it will be swept through all values in the array.
    # . If two arrays are longer than 1 you will get an error. The code only supports 2D sweeping.
    # . If no array is longer than 1, program measures time dependence.
    # . Any values in 'Parameters' dict will override those set in the 'Settings' dict.
    # . 'Settings' dict is for initialization settings only. This is passed to the __init__
    # . function of each device before sweeping begins.
    # . The network analyzer you want to sweep must have it's 'Device Name'
    # . in Settings['Device_to_frequency_sweep'], in case you have more than one.
    # . Check that GetData.py imports the device you want to sweep. ie 'from [filename] import [Name of driver class]
    # . Start by running GetData.py
    # . GetData.py will dynamically load all the modules it needs. Make sure the instrument
    # . class is saved in a file called '[Instrument].py' and the class is called [Instrument]
    # . All settings will be written to the logbook specified in the 'File' dict with plots.

    # Formatting for adding instruments and parameters:

    # 'Device Name': {'Address': VISA Resource Address,
    #            'Instrument': Name of driver class (e.g. 'NetworkAnalyzer', 'MG3692B',
    #            'Settings': {
    #                Setting1: Setting required in initialization eg SPAN or Averages
    #                SettingN: ...
    #            },
    #            'Parameters': {
    #
    #                Parameter1 Name':
    #                    {'SetFunction': Name of set Function,
    #                     'GetFunction': Name of get Function
    #                     'Array': np.linspace(start,stop,points}, Array of type (List) of values to be swept over.
    #                     If length 1 (e.g. [0] - it's just set to this value at the start
    #
    #                E5071C Center Frequency [Hz]':
    #                    {'SetFunction': 'SetCentralFrequency',
    #                     'GetFunction': 'GetCentralFrequency',
    #                     'Array': [32.716037e3]}
    #
    #            }
    #
    #            }
    Devices = {
        # 'E5071C': {'Address': 'TCPIP0::A-E5071C-14370::inst0::INSTR',
        #            'Instrument': 'NetworkAnalyzer',
        #            'Settings': {
        #                'Span': 100,
        #                'Points': 1601,
        #                'IFBW': 20,
        #                'Averages': 8,
        #                'CenterFrequency': 2.142261e6,
        #                'SignalLabel': '|S21|'
        #            },
        #            'Parameters': {
        #                # 'E5071C Power [dBm]':
        #                #     {'SetFunction': 'SetPower',
        #                #      'GetFunction': 'GetPower',
        #                #      'Array': np.arange(-10, -15, -0.1)
        #                         # np.concatenate((np.arange(2, -20, -0.5),
        #                         #                      np.arange(-20, 5, 0.5)), axis=0)
        #                     #},
        #                # 'E5071C Frequency [Hz]' :
        #                #     {
        #                #         'SetFunction': 'SetCentralFrequency',
        #                #         'GetFunction': 'GetCentralFrequency',
        #                #         'Array': np.linspace(2.138e6,2.15e6,1001)
        #                #     }
        #            }
        #            },

        # 'E5061B': {'Address': 'TCPIP0::K-E5061B-08566::inst0::INSTR',
        #            'Instrument': 'NetworkAnalyzer_PowerSweep',
        #           'Settings': {
        #               'SignalLabel': '|S21|'
        #           },
        #            'Parameters':{}
        #            },

        'E5061B': {'Address': 'TCPIP0::K-E5061B-08566::inst0::INSTR',
                   'Instrument': 'NetworkAnalyzer',
                   'Settings': {
                            'Span': 20e3,
                            'Points': 1601,
                            'IFBW': 1000,
                            'Averages': 1,
                            # 'SweepTime': 2000,
                            'CenterFrequency': 700e3,
                            'SignalLabel': '|S21|'
                            },
                   'Parameters': {
                       'E5061B Power [dBm]':
                           {'SetFunction': 'SetPower',
                            'GetFunction': 'GetPower',
                            'Array': [10]  # np.append(np.arange(0, -20, -0.1), np.arange(-20, 0, 0.1))
                                # np.ceil(100*np.log10(np.linspace(np.power(10, -5/10), np.power(10, -15/10), 41)))/10
                            },

                       'E5061B Frequency [Hz]':
                           {'SetFunction': 'SetCentralFrequency',
                            'GetFunction': 'GetCentralFrequency',
                            'Array': np.linspace(500e3,20e6,1001)  # np.append(np.arange(0, -20, -0.1), np.arange(-20, 0, 0.1))
                            # np.ceil(100*np.log10(np.linspace(np.power(10, -5/10), np.power(10, -15/10), 41)))/10
                            },
                       'RF Out':
                           {'SetFunction': 'SetOutput',
                            'GetFunction': 'GetOutput',
                            'Array': [1]}  # 3.3083063;3.4033502;3.4991063;3.5785861
                    }
                   },

        # 'Agilent33521B': {'Address': 'GPIB0::10::INSTR',
        #            'Instrument': 'Agilent33521B',
        #            'Settings': {
        #            },
        #            'Parameters': {
        #                'Agilent33521B Power [dBm]':
        #                    {'SetFunction': 'SetPower',
        #                     'GetFunction': 'GetPower',
        #                     'Array': np.arange(22, 0, -0.1)
        #                     },
        #                'Agilent33521B Frequency [Hz]':
        #                    {
        #                     'SetFunction': 'SetFrequency',
        #                     'GetFunction': 'GetFrequency',
        #                     'Array': [2.141e6] #np.linspace(2.142e6, 2.148e6, 301)2.14233e6 2.14235e6
        #                    }
        #            }
        #                   },

        'AmericanMagnetics': {'Address': 'TCPIP0::192.168.0.4::7180::SOCKET',
                              'Instrument': 'AmericanMagnetics430',
                              'Settings': {},
                              'Parameters': {
                                  'Field [T]':
                                      {'SetFunction': 'setField',
                                       'GetFunction': 'getField',
                                       'Array': [5]  # np.logspace(np.log10(0.2**2), np.log10(2.5**2), 50)**0.5
                                       # np.concatenate((np.arange(5, 0.5, 0.25)**0.5,
                                       #                      np.arange(0.5, 2, 0.1)**0.5), axis=0)
                                       }
                              }
                              },


        # 'N9030A': {'Address': 'TCPIP0::A-N9030A-80318::inst0::INSTR',
        #             'Instrument': 'N9030A_IQ',
        #             'Settings':{'CenterFrequency':2.1391e6,
        #                         'SampleRate': 2**10,
        #                        'BlockSize':2**18,
        #                         'TraceLength':2**18,
        #                         'SignalLabel':'V^2/Hz'
        #
        #             },
        #             'Parameters': {
        #
        #
        #         }

        # 'N9030A': {'Address': 'TCPIP0::A-N9030A-80318::inst0::INSTR',
        #             'Instrument': 'N9030A',
        #             'Settings':{'CenterFrequency': 2.142243e6,
        #                         'Span': 250,
        #                         'Points': 3001,
        #                         'Averages': 500,
        #                         'Bandwidth': 2,
        #                         'SignalLabel': 'dBm/Hz'
        #             },
        #             'Parameters': {
        #         }
        #         }}

        #  'E8257D': {'Address': 'TCPIP0::192.168.0.115::inst0::INSTR',
        #             'Instrument': 'E8257D',
        #              'Settings':{},
        #              'Parameters': {
        #
        #              'E8257D Power [dBm]':
        #                     {'SetFunction': 'SetPower',
        #                     'GetFunction': 'GetPower',
        #                 'Array': [0]
        #                       },
        #
        #              'E8257D Frequency [Hz]':
        #                      {'SetFunction': 'SetFrequency',
        #                       'GetFunction': 'GetFrequency',
        #                       'Array': np.linspace(2.115e6,2.12e6,101)
        #                       },
        #              }
        #         },
    }

    # Will try and find temperatures using the weblogging. If this fails defaults to the values below.
    try:
        http = urllib3.PoolManager()
        Temperature = {'MXC': float(http.request('GET',
                                                 'http://www.lancaster.ac.uk/pg/thompsom/MXC_Temp.js').data.split()[-2][
                                    1:-2]),
                       'Still': float(http.request('GET',
                                                   'http://www.lancaster.ac.uk/pg/thompsom/Still_Temp.js').data.split()[
                                          -2][1:-2]),
                       '4K': float(http.request('GET',
                                                'http://www.lancaster.ac.uk/pg/thompsom/FourK_Temp.js').data.split()[
                                       -2][1:-2])}
        # 'Magnet':float(http.request('GET',
        # 'http://www.lancaster.ac.uk/pg/thompsom/Magnet_Temp.js').data.split()[-2][1:-2])}
    except:
        Temperature = {'Start': 0.009,
                       'Stop': 0.009}

    Time = str(time.strftime("%Y-%m-%d-%H-%M-%S"))

    File = {'Data': 'F:/NEMSTuningFork/Data/Data/hdf5/' + Time,
            'DataNPZ': 'F:/NEMSTuningFork/DATA/Data/npz/' + Time,
            'Dir': 'F:/NEMSTuningFork/data/Data/',
            'Log': 'F:/NEMSTuningFork/data/Labbook/',
            'Plots': 'F:/NEMSTuningFork/data/Plots/Plot' + Time,
            'PlotDir': 'F:/NEMSTuningFork/data/Plots/',
            'PlotAnalysis': 'F:/NEMSTuningFork/data/Plots/Analysis/',
            'Labbook': 'Labbook4.Tex'
            }

    Settings = {'Devices': Devices,
                'Device_to_Frequency_Sweep': 'E5061B',
                # Specifies which network analyzer (or spectrum analyzer) to f-sweep
                'Temperature': Temperature,
                'Measurements': Measurements,
                'File': File,
                'Time': Time,
                'SavePlotsEvery': 10
                }
    return Settings
