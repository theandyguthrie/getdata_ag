# -*- coding: utf-8 -*-
"""
Created on Tue Oct 11 12:38:05 2016

@author: wilcox
"""

import visa
import numpy as np

class SR830:
    
    def __init__(self, Address, Settings ):
        rm = visa.ResourceManager()
        rm.list_resources()
        self.dev = rm.open_resource(Address)
        self.dev.write('OUTX 1') #Sets device to communicate via GPIB

        self.sensitivities = [100e-9, 300e-9, 1e-6, 3e-6, 10e-6, 30e-6, 100e-6, 300e-6, 1e-3, 3e-3, 10e-3, 30e-3, 100e-3, 300e-3, 1.]
        self.time_constants = [100e-6, 300e-6, 1e-3, 3e-3, 10e-3, 30e-3, 100e-3, 300e-3, 1., 3., 10., 30., 100., 300., 1.e3, 3.e3, 10.e3, 30.e3]
        self.set_time_constant(Settings['TimeConstant'])
        self.set_sensitivity(Settings['Sensitivity'])

        self.set_phase(Settings['Phase'])
        self.set_offsets(Settings['Chan1Offset'],Settings['Chan2Offset'])
        print(['Initialised: ', self.dev.query('*IDN?')])

        return
    
    def measure(self):
        x = self.dev.query('OUTP? 1')
        y = self.dev.query('OUTP? 2')
        return x, y
    
    def use_external_reference(self):
        self.dev.write('FMOD 0')
        return
         
    def use_internal_reference(self):
        self.dev.write('FMOD 1')
        return
        
    def set_reserve(self, Reserve): # 0 for high reserve, 1 for normal
                                   #, 2 for low noise 
        self.dev.write('RMOD ' + str(Reserve))
        return
    
    def set_harmonic(self, HarmNo):
        self.dev.write('HARM ' + str(HarmNo))
        return
    
    #Enter an arbitrary number and the script will round up to the closest 
    #valid sensitivity.    
    def set_sensitivity(self, Sens):
        for i, sens in enumerate(self.sensitivities):
            if sens >= Sens:
                break

        self.dev.write('SENS ' + str(i))



        return

    def set_time_constant(self, Time):
        for i, time_const in enumerate(self.time_constants):
            if time_const >= Time:
                break

        self.dev.write('OFLT ' + str(i))

    def set_phase(self, phase):
        self.dev.write('PHAS ' + str(phase))

    def set_offsets(self, X,Y):
        self.dev.write('DOFF 1,0,' + str(X))
        self.dev.write('DOFF 2,0,' + str(Y))


    def get_phase(self):
        return self.dev.write('PHAS?')
        
    def get_sensitivity(self):
        SensID = self.dev.query('SENS?')
        powers = np.logspace(-9, -1, num=9, base=10.0)
        fractions = np.array([2, 5, 10]) 
        Sensitivities = np.zeros(27)
        for i in range(9):
            for j in range(3):
                Sensitivities[(j + (3*i))] = fractions[j]*powers[i]       
        return Sensitivities[int(SensID)]

    def return_sens_ID(self):
        SensID = self.dev.query('SENS?')
        return SensID
                             