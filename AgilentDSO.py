import numpy as np
import visa
import time
# import mod_oscillator as osc
class AgilentDSO:
    def __init__(self,Address,Settings):
        rm = visa.ResourceManager()
        rm.list_resources()
        self.dev = rm.open_resource(Address)

        time.sleep(0.2)
        self.dev.write(':TRIG:SWE AUTO')
        time.sleep(0.2)
        self.sampleRate = int(self.dev.query(':ACQ:SRAT?'))
        time.sleep(0.2)
        # self.dev.write(':TIM:POS ' + str(Settings['Delay']))
        # time.sleep(0.2)
        # self.dev.write(':TIM:SCAL ' + str(Settings['TimeSpan']))
        # time.sleep(0.2)
        # self.dev.write(':TRIG:EDGE:SLOP ' + Settings['TriggerSlope'])
        # time.sleep(0.2)
        # self.dev.write(':CHAN1:SCAL' + str(Settings['Chan1Scale']))
        # time.sleep(0.2)
        # self.dev.write(':CHAN2:SCAL ' + str(Settings['Chan2Scale']))
        time.sleep(0.2)
        self.dev.write(':STOP')
        self.dev.write(':TRIG:SWE AUTO')

        #Set Ascii (can this be changed to binary?)
        self.dev.write(':WAV:FORM ASC')

        #self.dev.__setattr__('VI_ATTR_TMO_VALUE', '2000')

        self.t10 = 0
        #self.dev.write(':TRIG:LEV ' + str(Settings['TriggerLevel']) +',CHAN1')




        print(['Initialised: ', self.dev.query('*IDN?')])






    def GetTrace(self):
        print('Waiting for event on ' + str(self.dev.query('*IDN?')))
        self.dev.write(':STOP')



        #xUnits = self.dev.query(':WAV:XUN?')
        #print(xUnits)



        #xOrigin = float(self.dev.query(':WAV:XOR?'))
        #xIncrement = float(self.dev.query(':WAV:XINC?'))
        #xPoints = int(self.dev.query(':WAV:POIN?'))

        self.dev.write('*CLS')
        self.dev.clear()



        self.dev.write(':WAV:SOUR CHAN1')
        time.sleep(0.1)
        print(time.time() - self.t10)
        self.dev.write(':SING')

        self.dev.query(':TER?')
        while int(self.dev.query(':TER?')[1:])!=1:
            time.sleep(0.01)

        while int(self.dev.query('*OPC?'))!=1:
            time.sleep(0.01)

        trigtime = float(time.time())
        self.t10=time.time()

        #??? Seems to timeout when reading in NORM mode
        self.dev.write(':STOP')
        self.dev.write(':TRIG:SWE AUTO')
        self.dev.write(':STOP')

        #To separate ascii values, should be changed to byte at some point but I can't figure out the header?
        print('Collecting data for channel 1')
        sReal = np.array(self.dev.query(':WAV:DATA?')[10:].split(','),dtype='f')

        self.dev.write(':WAV:SOUR CHAN2')

        print('Collecting data for channel 2')

        sImag = np.array(self.dev.query(':WAV:DATA?')[10:].split(','),dtype='f')
        xData = [1/(self.sampleRate) * i for i in range(len(sReal))]
        self.dev.write(':TRIG:SWE NORM')






        return {'Trace_xdata':xData, 'sReal':sReal, 'sImag':sImag,'TriggerTime':trigtime}


    def getSampleRate(self):

        return np.double(self.dev.query(':ACQ:SRATE?'))









