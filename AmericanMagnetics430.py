import numpy as np
import visa
import pyvisa.highlevel
import time

class AmericanMagnetics430:


    def __init__(self, Address, Settings1):
        rm = visa.ResourceManager()
        rm.list_resources()



        self.dev = rm.open_resource(Address)

        self.dev.read_termination = '\r\n'
        self.dev.write_termination = '\r\n'

        self.Settings = Settings1

        print(['Initialised: ', self.dev.query('*IDN?')])


    def setPersistanceMode(self, value):
        self.dev.write('PSwitch '+ str(value) + ';')


    def getField(self):
        # value = 0
        # out = 0
        # while (value == 0) or out==2:
        #     try:
        #         out = float(self.dev.query('FIELD:MAG?;'))
        #         value = 1
        #     except TypeError:
        #         time.sleep(0.01)

        return self.field



    def setField(self,Field):

        print('Sweeping field to ' + str(Field) + 'T')
        self.field = Field


        self.dev.write('CONF:FIELD:TARG ' + str(Field) + ';')

        self.dev.write('RAMP;')

        self.dev.query('STATE?;')

        time.sleep(0.5)

        self.dev.query('STATE?;')

        a=0
        while a==0:
            try:
                if (np.int(self.dev.query('STATE?;')) == 2):
                    a = 1
            except pyvisa.errors.VisaIOError:
                time.sleep(0.5)
                a = 0


            time.sleep(1)




    def setRate(self,Rate):

        1==1

