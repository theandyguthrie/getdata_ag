import numpy as np
import visa
import time

class E8257D:

    def __init__(self, Address, Settings):
        rm = visa.ResourceManager()
        rm.list_resources()


        self.dev = rm.open_resource(Address)


        print(['Initialised: ', self.dev.query('*IDN?')])


        return


    def SetPower(self, Power):

        print('Setting power to ' + str(Power) + 'dBm on ' + str(self.dev.query('*IDN?')))

        self.dev.write(':POW ', str(Power))

        out = np.double(self.dev.query(':POW?'))
        return out


    def GetPower(self):

        out = np.double(self.dev.query(':POW?'))
        return out


    def SetFrequency(self, Frequency):


        print('Setting frequency to ' + str(Frequency) + 'Hz on ' + str(self.dev.query('*IDN?')))

        self.dev.write(':FREQ ', str(Frequency))

        out = np.double(self.dev.query(':FREQ?'))
        return out

    def GetFrequency(self):
        out = np.double(self.dev.query(':FREQ?'))
        return out


    def SetOutput(self,output):

        self.dev.write('OUTP:STAT ' + output)




