import visa
import numpy as np
import time

# By Joshua Chawner
# V1.0 ~ Known to work quite well.

rm = visa.ResourceManager()
rm.list_resources()


class MG3692B():
    def __init__(self, GPIB, Settings):
        self.address = GPIB
        self.dev = rm.open_resource(self.address)


        self.dev.clear()



        print(['Initialised: ', self.dev.query('*IDN?')])


    def Set_Output(self,Output):

        if Output:
            Output=1
        else:
            Output=0


        self.dev.write('RF ' + str(Output))

    def Set_Frequency(self, Frequency):

        print('Setting Frequency to ' + str(Frequency) + 'Hz on ' + str(self.dev.query('*IDN?')))

        freq = float(Frequency/1e9)

        self.dev.write('F1' + str(freq) + ' GH')

    def Get_Frequency(self):

        # done = 1
        #
        # while done ==1:
        #     try:
        #         out = float(self.dev.query('OF1'))*1000000
        #         done = 0
        #     except ValueError:
        #         done = 1

        out = 1
        while out==1:
            try:
                out = float(self.dev.query('OF1'))*1000000
            except ValueError:
                out = float(self.dev.query('OF1')) * 1000000

        return out


    #Power can only be set to the nearest 0.01dBm. Settings more precise than this are rounded.
    def SetPower(self, Power):


        print('Setting power to ' + str(Power) + 'dBm on ' + str(self.dev.query('*IDN?')))

        self.dev.write('XL1 ' + str(Power) + 'DM')


    def GetPower(self):

        a = False

        while a==False:
            try:
                out = float(self.dev.query('OL1'))
                a = True
            except ValueError:
                a = False
            time.sleep(0.1)


        return out


    def SetPhase(self,phase):

        print('Setting phase to ' + str(phase) + 'degrees on ' + str(self.dev.query('*IDN?')))

        self.dev.write('PS1')
        self.dev.write('PSO ' + str(int(phase)) + ' DG')

    def GetPhase(self):


        a = False

        while a==False:
            try:
                out = float(self.dev.query('OPO'))
                a = True
            except ValueError:
                a = False
            time.sleep(0.1)


        return out



