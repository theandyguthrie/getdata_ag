import visa
import numpy as np
import time

rm = visa.ResourceManager()
rm.list_resources()

class Keithley2000():
    def __init__(self, GPIB, Settings):

        self.address = GPIB
        self.inst = rm.open_resource(self.address)
        self.settings = Settings
        self.inst.write('*CLS')
        self.inst.write('*RST')
        self.inst.write(':CONF:VOLT:DC')
        self.inst.write(':VOLT:DC:RANG:AUTO ON')
        self.inst.write(':VOLT:DC:DIG 7')
        self.inst.write(':VOLT:DC:AVER:TCON REP')
        self.inst.write(':VOLT:DC:AVER:COUN ' + str(Settings['Aver']))
        self.inst.write(':VOLT:DC:AVER:STAT ON')
        self.inst.write(':FORM:DATA SREAL')
        self.inst.write(':FORM:BORD SWAP')
        print(['Initialised: ', self.inst.query('*IDN?')])

    def ping(self):
        return self.inst.query("*IDN?")

    def _RESET(self):
        self.inst.write("*RST")

    def _READ(self):
        self.inst.write('*CLS; INIT; *OPC?')
        while self.inst.stb == 0:
            time.sleep(0.1)
        print('The data ready for collection: OPC=' + self.inst.read())
        x = float(self.inst.query_values(":FETC?")[0])/self.settings['Ampl']
        return x


