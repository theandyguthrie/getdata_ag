import numpy as np
import visa
import pyvisa.highlevel
import time
# import mod_oscillator as osc
class N9030A_IQ:
    def __init__(self,Address,Settings):
        rm = visa.ResourceManager()
        rm.list_resources()
        self.dev = rm.open_resource(Address)
        self.dev.chunk_size = 2**20
        self.dev.timeout = 2**16
        # Set instrument into I/Q BASIC measurements mode
        self.dev.write(':INST:SEL BASIC')
        # Set instrument to measure I/Q time trace and RF envelope
        self.dev.write(':CONF:WAV')
        # Define output format
        self.dev.write(':FORM:DATA REAL')
        self.dev.write(':FORM:BORD SWAP')
        # Set frequency.
        self.centerfreq = float(Settings['CenterFrequency'])
        self.dev.write(':SENS:FREQ:CENT ', str(self.centerfreq))
        # Set sample rate
        self.sampleRate = float(Settings['SampleRate'])
        self.dev.write(':WAV:SRAT ' + str(self.sampleRate))
        # Set recording length for fast capture record
        self.traceLength = float(Settings['TraceLength'])
        self.dev.write(':FCAP:LENG ' + str(self.traceLength))
        # Set block size for reading data from the record
        self.blockSize = float(Settings['BlockSize'])
        self.dev.write(':FCAP:BLOC ' + str(self.blockSize))

        # self.span = float(Settings['Span'])
        # self.dev.write(':SENS:SPEC:FREQ:SPAN ', str(self.span))
        # Set meaurements bandwidth
        # self.bandwidth = float(Settings['Bandwidth'])

        # # Set Averages
        # self.dev.write(':SENS:SPEC:AVER 0 ')
        # Confirm inicialization
        print(['Initialised: ', self.dev.query('*IDN?')])
        time.sleep(0.1)
        return

    def GetTrace(self):
        print('Sweeping ' + str(self.dev.query('*IDN?')))
        time.sleep(0.1)
        # Request the sampling rate
        SamplingRate = float(self.dev.query(':WAV:SRAT?'))
        self.sampleRate = SamplingRate

        #Time Array = np.array(range(len(I))) / Fs
        # Initiate fast capture measurements
        self.dev.write('*CLS; INIT:FCAP; *OPC?')
        #wait until measurment is finished
        while self.dev.stb == 0:
            time.sleep(0.1)
        #read data
        print('The data ready for collection: OPC=' + self.dev.read())
        print('Collecting data ...')
        RawData = []
        pointer = float(self.dev.query(':SENS:FCAP:POIN?'))
        while pointer < (float(self.traceLength)):
            RawData = np.concatenate((RawData,self.dev.query_values('FETC:FCAP?')),axis=0)
            pointer = float(self.dev.query(':SENS:FCAP:POIN?'))

        #RawData = self.dev.query_values('FETC:FCAP?')
        #Separating I/Q
        Idata = np.array(RawData[0:len(RawData):2])
        Qdata = np.array(RawData[1:len(RawData):2])


        phase, frequency, magnitude = [], [], []
        frequency = np.double(self.dev.query(':SENS:FREQ:CENT?'))
        for y in range(0,len(Idata)):
            magnitude.append(pow(Idata[y]**2 + Qdata[y]**2, 0.5))
            if Idata[y] != 0 :
                phase.append(np.arctan(Qdata[y] / Idata[y]))
            else:
                phase.append(np.pi/2)
        sTime = np.array(range(len(Idata))) / SamplingRate

        # Read frequency data

        data = {'Frequency': frequency, 'sReal': np.array(Idata), 'sImag': np.array(Qdata),'Magnitude': np.array(magnitude), 'Phase': np.array(phase), 'sTime':np.array(sTime)}
        return data


    def GetPower(self):

        out = -9999
        return out

    def GetVoltage(self):

        out = np.double(self.dev.query(':SOUR:POW:LEV:IMM:AMPL?'))
        return out

    def SetCentralFrequency(self, Frequency):

        self.dev.write(':SENS:FREQ:CENT ', str(Frequency))
        time.sleep(0.1)
        self.centerfreq = Frequency

        out = np.double(self.dev.query(':SENS:FREQ:CENT?'))
        return out

    def GetCentralFrequency(self, Frequency):

        out = np.double(self.dev.query(':SENS:FREQ:CENT?'))
        return out

    def SetSpan(self, Span):

        # Set frequency span.
        self.dev.write(':SENS:FREQ:SPAN ', str(Span))
        time.sleep(0.1)
        self.span = Span

        out = np.double(self.dev.query(':SENS:FREQ:SPAN?'))
        return out

    def GetSampleRate(self):

        return self.sampleRate

    def ReadBandwidth(self):

        return np.double(self.dev.query(':SENS:BAND:RES?'))

