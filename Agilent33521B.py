import numpy as np
import scipy as sc
import matplotlib as plt
import visa
import pyvisa.highlevel
import time

class Agilent33521B:

    def __init__(self,Address,Settings):
        rm = visa.ResourceManager()
        rm.list_resources()

        self.dev = rm.open_resource(Address)

        self.dev.read_termination = '\n'
        self.dev.write_termination = '\n'
        self.dev.__setattr__('VI_ATTR_SEND_END_END', 'VI_False')




        print(['Initialised: ', self.dev.query('*IDN?')])
        time.sleep(0.1)

        return



    # def GetVoltage(self):
    #
    #     out = np.double(self.dev.query(':SOUR:POW:LEV:IMM:AMPL?'))
    #     return out

    def SetFrequency(self, Frequency):
        self.dev.write(':FREQ ', str(Frequency))

    def GetFrequency(self):
        return np.double(self.dev.query(':FREQ?'))



    def SetVoltage(self,Voltage):
        print('Setting voltage to ' + str(Voltage) + 'V on ' + str(self.dev.query('*IDN?')))
        self.dev.write(self.dev.write('APPL:DC DEF,DEF, ' + str(Voltage) + 'V'))


    def GetPower(self):
        return np.double(self.dev.query('SOUR:VOLT?'))

    def SetPower(self,power):
        self.dev.write('SOUR:VOLT:UNIT\sDBM')

        print('Setting power to ' + str(power) + 'dBm on ' + str(self.dev.query('*IDN?')))
        self.dev.write('SOUR:VOLT ' + str(power))


    def GetVoltage(self):
        return np.double(self.dev.query('VOLT?'))
