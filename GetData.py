# Author - Andrew Guthrie
print('Initializing classes:')
from PyQt5 import QtWidgets
print('PYQT5.py')
from MeasurementSettings1 import MeasurementSettings1
print('MeasurementSettings.py')
import sys
print('sys.py')
import mod_SweepPYQTGRAPH
print('mod_SweepPYQTGRAPH_IQ.py')
import importlib
print('importlib.py')
import time

# Create dictionary of settings
Settings = MeasurementSettings1()
Devices = {}
# Populate Device dictionary with each Device object and Settings
for key in Settings['Devices'].keys():
    # Loads instrument class [Instrument] from [Instrument].py
    print(Settings['Devices'][key]['Instrument'] + '.py')
    mod = importlib.import_module(Settings['Devices'][key]['Instrument'])
    instrumentClass = getattr(mod, Settings['Devices'][key]['Instrument'])
    # Passes (Address, Settings) to device and then saves [InstrumentObject, Settings) as dict
    Devices[key] = [instrumentClass(Settings['Devices'][key]['Address'],
                                    Settings['Devices'][key]['Settings']),
                    Settings['Devices'][key]]

# Sweep!  Starts instance of plot_app software which then threads the data collection script.
if __name__ == '__main__':
    # Run app
    app = QtWidgets.QApplication(sys.argv)
    pl = mod_SweepPYQTGRAPH.plot_app(Settings, Devices=Devices, Name='')
    sys.exit(app.exec_())
