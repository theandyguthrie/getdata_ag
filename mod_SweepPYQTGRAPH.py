import sys
from PyQt5 import QtWidgets, QtCore, QtGui
import pyqtgraph as pg
from mod_fileio_1 import Output_h5py, OutputLabbook1
import pyqtgraph.exporters
import numpy as np
import time
import h5py


# Class for the plotting software using PyQtGraph
class plot_app(QtWidgets.QWidget):
    # Takes the Devices dict Settings dicts and stores these in the class.
    def __init__(self, Settings, Devices=None, Name=''):
        # Setup GUI
        super(plot_app, self).__init__()
        self.Settings = Settings
        self.Devices = Devices
        self.Name = Name
        self.init_ui()
        self.show()
        self.go()

# Creates all the basic graphs. One colormap with histogram and one standard plot.
    def init_ui(self):
        self.last_ind_plot = None
        # Sets colors and fonts for plots
        pg.setConfigOptions(background='w', foreground='k')
        labelStyle = {'font-family': "calibri", 'font-size': '18px'}
        self.labelStyle = labelStyle
        titleStyle = {'size': '18px'}
        self.titleStyle = titleStyle
        font = QtGui.QFont()
        font.setPixelSize(15)

        # Creates colormap style plot (called ImageItem in PyQtGraph). Puts this plot inside a widget
        img1 = pg.ImageItem()
        plot_widget = pg.PlotWidget()
        plot_widget.addItem(img1)
        self.img1 = img1
        self.main_plot = plot_widget.getPlotItem()

        # Sets title to measurement type from settings
        self.main_plot.showButtons()
        plot_widget.setLabel('left', 'Frequency (Hz)', **labelStyle)
        plot_widget.setTitle(self.Settings['Measurements']['MeasurementsType'], **titleStyle)

        # Get plot widget for plot 2 (regular line plot)
        plot_widget2 = pg.PlotWidget()
        self.plot2 = plot_widget2.getPlotItem()

        # Sets yaxis label to the ['SignalLabel'] from the device being swept. (i.e |S21| for network analyzers)
        # Sets x axis label
        self.plot2.showButtons()
        Signallabel = self.Devices[self.Settings['Device_to_Frequency_Sweep']][1]['Settings']['SignalLabel']
        plot_widget2.setLabel('left', Signallabel, **labelStyle)
        plot_widget2.setLabel('bottom', 'Frequency (Hz)', **labelStyle)

        # Hist object is the sliding color scale in PyQtGraph. You can change the colors by changing loadPreset
        # Puts the hist object inside a widget.
        hist = pg.HistogramLUTWidget()
        hist.setImageItem(img1)
        hist.gradient.loadPreset('spectrum')

        # Define layouts and sets location and size
        win = QtGui.QWidget()
        self.win = win
        self.setGeometry(0, 30, 960, 1170)

        # Creates a grid inside the large widget. Puts first plot in row1,col1, plot2 in row2, col1, and hist in col2.
        # Scales col1 so it is 15x bigger than col2. (Makes the colorbar look good)
        layout = QtGui.QGridLayout()
        win.setLayout(layout)
        self.setLayout(layout)
        layout.addWidget(plot_widget, 0, 0, 1, 1)
        layout.addWidget(plot_widget2, 1, 0, 1, 1)
        layout.addWidget(hist, 0, 1, 2, 1)
        layout.setColumnStretch(0, 15)
        layout.setColumnStretch(1, 1)

        # Sets fonts to the font defined above
        plot_widget.getAxis("bottom").tickFont = font
        plot_widget.getAxis("bottom").labelFont = font
        plot_widget.getAxis("left").tickFont = font
        plot_widget2.getAxis("bottom").tickFont = font
        plot_widget2.getAxis("left").tickFont = font

        self.NumberofTicks = 10
        self.TickDividers = [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000]

    # This will be started as a thread by initui and controls the plotting.
    # The pyqtSlot defines the data types that are taken from the other thread
    # These are cast straight to the variables in the def(...)
    @QtCore.pyqtSlot(np.ndarray, np.ndarray, np.ndarray, str, int, int, str)
    def plot(self, x, y, f, label, ind, arrayLength, timeLeftString):
        self.t0 = time.time()
        self.ind = ind

        # Finds upper and lower bounds of the zdata
        plot_min_z = np.amin(y)
        plot_max_z = np.amax(y)

        # Sets the axis labels to the xdata parameter data
        self.main_plot.setLabel('bottom', label, **self.labelStyle)
        self.plot2.setTitle('Plot ' + str(self.ind + 1) + ' of ' + str(arrayLength) +
                            ' - ' + timeLeftString, **self.titleStyle)

        # Creates a linear array of the 'run' values to plot against
        # inds = np.linspace(0,ind,ind+1)

        # Scales the image on the colormap by the size of the frequency and xparam data
        self.img1.resetTransform()
        f_scale = (np.amax(f) - np.amin(f)) / y.shape[1]
        # if f_scale==0:
        #     f_scale=1

        x_scale = 1  # (np.amax(inds) - np.amin(inds)) / y.shape[0]
        x_offset = -0.5  # np.amin(inds)
        f_offset = np.amin(f)

        # Moves the raw rectangle to the lowest x value and f values.
        # Scales by the actual span of each of the axes
        self.img1.translate(x_offset, f_offset)
        self.img1.scale(x_scale, f_scale)

        # Plots data
        self.img1.setImage(y, levels=[plot_min_z, plot_max_z], autolevels=False)

        # Swaps the 'run' labels for the actual x values as strings.
        # This was done to allows up and down sweeping for the color plot
        if len(x) > 1:
            Strings = self.main_plot.getAxis('bottom').tickStrings(x, 1, np.abs(x[0]-x[1]))
            tickStrings = [[]]
            for index in range(0, ind+1):
                tickStrings[0].append((index, Strings[index]))

            tickStringstemp = tickStrings[0]
            ind = 0
            while len(tickStrings[0]) > self.NumberofTicks:
                tickStrings[0] = tickStringstemp
                tickStrings[0] = tickStrings[0][::self.TickDividers[ind]]
                ind += 1

            self.main_plot.getAxis('bottom').setTicks(tickStrings)

        # plots last sweep as a function of frequency
        self.plot2.plot(f, y[-1], clear=True, pen=(255, 165, 0))

        # Saves image of both plots once every specified number of sweeps
        if (self.last_ind_plot != self.ind) and (self.ind%self.Settings['SavePlotsEvery'] == 0):
            self.last_ind_plot = self.ind
            self.saveImages()

    # Starts the three threads and connects the slow to the signal
    def go(self):
        self.th1 = savingThread({}, {}, '')

        self.th = random_thread(self.Settings, self.th1, self.Devices, self.Name)
        self.th.new_data.connect(self.plot)     # Connect thread signal to plot function in main GUI

        self.th.datatosave.connect(savingThread.run)
        self.th.start()

    # Updates plot and saves two images to the locations specified in the file.
    # Here you can change the x and y size of the plots.
    def saveImages(self):
        QtGui.QApplication.processEvents()

        self.exporter = pg.exporters.ImageExporter(self.main_plot)
        self.exporter2 = pg.exporters.ImageExporter(self.plot2)

        # set export parameters if needed
        self.exporter.params.param('width').setValue(960, blockSignal=self.exporter.widthChanged)
        self.exporter.params.param('height').setValue(500, blockSignal=self.exporter.heightChanged)
        self.exporter2.params.param('width').setValue(960, blockSignal=self.exporter2.widthChanged)
        self.exporter2.params.param('height').setValue(500, blockSignal=self.exporter2.heightChanged)
        # save to file

        self.exporter.export(str(self.Settings['File']['Plots'] + self.Name + '3D.png'))
        self.exporter2.export(str(self.Settings['File']['Plots'] + self.Name + '2D.png'))


# This object contains the thread for getting the data.
class random_thread(QtCore.QThread):
    new_data = QtCore.pyqtSignal(np.ndarray, np.ndarray, np.ndarray, str, int, int, str)
    datatosave = QtCore.pyqtSignal(dict, dict, str)

    def __init__(self, Settings, savingThread, Devices=None, Name=''):
        super(random_thread, self).__init__()
        self.Devices = Devices
        self.Settings = Settings
        self.Name = Name
        self.savingThread = savingThread

    def run(self):
        #
        frequency = []
        sReal = []
        sImag = []
        magnitude = []
        phase = []
        run = []
        time1 = []
        sTime = []

        Devices = self.Devices
        Settings = self.Settings
        Name = self.Name

        x = {}
        Array = []

        # Find which parameter array has length > 1
        for key in list(Devices.keys()):
            for param in Devices[key][1]['Parameters'].keys():
                if Devices[key][1]['Parameters'][param]['Array'] is not None:
                    if len(Devices[key][1]['Parameters'][param]['Array']) > 1:
                        if len(Array) == 0:
                            Array = [key, Devices[key][1]['Parameters'][param]]
                            label = param
                        else:
                            print('This code only supports 2D sweeps')
                            sys.exit()
                x[param] = []
        ind = 0
        if not Array:
            Array = ['', {'Array': [0]}]
            label = 'Run'

        t0 = int(time.time())

        # Loops for length of the control array
        while ind < len(Array[1]['Array']):
            run.append(ind)

            if ind == 0:
                print('Saving to ' + str(Settings['File']['Data']))

                print('Setting all parameters to their starting values:')
                # Sets all device parameters to their starting values
                for key in list(Devices.keys()):
                    for param in Devices[key][1]['Parameters'].keys():
                        if Devices[key][1]['Parameters'][param]['SetFunction'] is not None:
                            method_to_call = getattr(Devices[key][0], Devices[key][1]['Parameters'][param]['SetFunction'])
                            method_to_call(Devices[key][1]['Parameters'][param]['Array'][0])

            # Sets control parameter once per sweep
            if Array[0] == '':
                Array[1]['Array'].append(1)
            else:
                method_to_call = getattr(Devices[Array[0]][0], Array[1]['SetFunction'])
                method_to_call(Array[1]['Array'][ind])

            # Reads all instrument parameters to a dictionary
            for key in Devices.keys():
                for param in Devices[key][1]['Parameters'].keys():
                    x[param].append(getattr(Devices[key][0], Devices[key][1]['Parameters'][param]['GetFunction'])())

            # Reads NetworkAnalyzer Trace
            data1 = Devices[Settings['Device_to_Frequency_Sweep']][0].GetTrace()

            # Appends all networkanalyzer values to the output
            frequency.append(data1['Frequency'])
            sReal.append(data1['sReal'])
            sImag.append(data1['sImag'])
            magnitude.append(data1['Magnitude'])
            phase.append(data1['Phase'])
            time1.append(int(time.strftime("%Y%m%d%H%M%S")))

            # Data1 is the dict to be saved. It only stores the last sweep of the 2D arrays to append to the hdf5 file
            data1['Run'] = run
            data1['Time1'] = time1

            data = {'Frequency': frequency,
                    'sReal': sReal,
                    'sImag': sImag,
                    'Phase': phase,
                    'Magnitude': magnitude,
                    'Run': run,
                    'Time1': time1,
                    'Settings': Settings}

            # Adds a value to the output dictionary for each of the instrument parameters
            for key in (x.keys()):
                data[key] = x[key]
                data1[key] = x[key]

            sweeptime = (time.time() - t0) / (ind + 1)

            # Outputs data to labbook and saves first f-sweep for presentation
            if ind == 3:
                print('Outputting to labbook')
                OutputLabbook1(Settings)

            Timeleft = np.round(sweeptime * (len(Array[1]['Array']) - (ind + 1)), 2)

            if Timeleft < 60:
                Timeleft = np.round(Timeleft, 2)
                timeLeftString = str(Timeleft) + ' seconds left'
            if Timeleft > 3600:
                Timeleft = np.round(Timeleft / 3600, 2)
                timeLeftString = str(Timeleft) + ' hours left'
            if (Timeleft < 3600) and (Timeleft > 60):
                Timeleft = np.round(Timeleft / 60, 2)
                timeLeftString = str(Timeleft) + ' minutes left'

            # Send data back to the plotting thread
            self.new_data.emit(np.array(data[label]), np.array(data['Magnitude']), np.array(data['Frequency'][0]),
                               label, ind, len(Array[1]['Array']), timeLeftString)
            # Emit the signal to signal the main gui to do plotting

            # Saves the data
            self.save(Settings, data1, Name='')
            self.save(Settings, data1, Name='1')

            ind += 1

    def save(self, Settings, data, Name=''):
        if data['Run'][-1] == 0:
            out = h5py.File(Settings['File']['Data'] + '-' + Name + '.hdf5', 'w')
            for key in data:
                if type(data[key]) == list:

                    out.create_dataset(key, data=data[key], chunks=True, maxshape=(None,))
                else:
                    out.create_dataset(key, data=[data[key]], chunks=True, maxshape=(None, None))
            out.close()

        else:
            out = h5py.File(Settings['File']['Data'] + '-' + Name + '.hdf5', 'r+')
            for key in data:
                if len(out[key].maxshape) == 1:
                    out[key].resize((len(data[key])), axis=0)
                    out[key][:] = data[key]
                else:
                    out[key].resize((len(data['Run']), len(data[key])))
                    out[key][-1] = data[key]

            out.close()


class savingThread(QtCore.QThread):     # Typically needs to be a PyQt object for signals to work
    def __init__(self, Settings, data, Name):
        super(savingThread, self).__init__()
        self.Settings = Settings
        self.data = data
        self.Name = Name

    @QtCore.pyqtSlot(dict, dict, str)
    def setStuff(self, Settings, data, Name):
        self.Settings = Settings
        self.data = data
        self.Name = Name

    def run(self):

        if self.data:
            Output_h5py(self.Settings, self.data, self.Name)
            Output_h5py(self.Settings, self.data, self.Name + '1')
